//
//  AppDelegate.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var server: FireServer!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        AuthService.onDidFinishLaunching()
        server = FireServer()
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        AuthService.handleOpenURL(app, open: url, options: options)
        return true
    }
 

}


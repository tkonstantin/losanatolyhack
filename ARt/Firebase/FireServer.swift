//
//  FireServer.swift
//  ARt
//
//  Created by Константин on 10/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore


class FireServer {
    
    init() {
        addSnapshotListener()
    }
    
    lazy var baseQuery: Query = {
        return Firestore.firestore().collection("murals")
    }()
    
    private var muralsListener: ListenerRegistration?
    
    
    private func addSnapshotListener() {
        baseQuery.addSnapshotListener { (snapshot, error) in
            var completed = 0
            var items: [ArtObject] = []
            
            snapshot?.documents.forEach({
                if let object = ArtObject(json: $0.data(), id: $0.documentID, completion: {
                    completed += 1
                    if completed == items.count, items.count != 0 {
                        HlocalStorage.shared.items = items
                    }
                }) {
                    items.append(object)
                }
            })

    
            if let error = error {
                print(#function, error)
            }
        }
    }
    
    func loadCatalogue(completion: @escaping ([CatalogueItem])->Void) {
        Firestore.firestore().collection("catalogue").addSnapshotListener { (snapshot, error) in
            completion(snapshot?.documents.compactMap({CatalogueItem(json: $0.data(), id: $0.documentID)}) ?? [])
        }
    }
}

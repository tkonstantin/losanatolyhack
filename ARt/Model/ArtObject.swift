//
//  LiveLivePhoto.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.

import UIKit
import ARKit
import SDWebImage
import Alamofire
import FirebaseStorage

class ArtObject {
    
    var id: String!
    var image: UIImage!
    var title: String!
    var videoPath: String! {
        get {
            return FileManager.videoPath(with: self.videoRemoteURL)
        }
    }
    var videoRemoteURL: URL!
    var imageRemoteURL: URL!
    var avatarURL: String!
    var owner_id: String!
    var item_id: String!
    
    var post: VK_Post? {
        didSet {
            if post != nil {
                onPostArrived?()
            }
        }
    }
    
    var onPostArrived: (()->Void)? = nil {
        didSet {
            if post != nil {
                onPostArrived?()
            }
        }
    }
    
    
    lazy var referenceImage: ARReferenceImage? = {
        guard let cgImg = image?.cgImage else { return nil }
        let img = ARReferenceImage(image.cgImage!, orientation: image.cgOrientation(), physicalWidth: 0.5)
        img.name = self.id
        return img
    }()
    
    init() { }
    
    init?(json _json: [String: Any]?, id _id: String, completion: @escaping ()->Void) {
        guard let json = _json else { return nil }
        id = _id
        title = json["title"] as? String ?? ""
        videoRemoteURL = URL(string: json["video"] as? String ?? "")
        owner_id = json["owner_id"] as? String ?? ""
        item_id = json["item_id"] as? String ?? ""
        avatarURL = json["avatar"] as? String ?? ""
        
        VK_Proxy.request(VK_Proxy.methods.wall.getByID, parameters: [.posts : "\(owner_id!)_\(item_id!)"], results: { json in
            self.post = VK_Post(json: json)
        }, completion: { success, errorMessage in
            print("vk get post \(success, errorMessage)")
        })
        
        
        imageRemoteURL = URL(string: json["image"] as? String ?? "")
        
        if videoRemoteURL == nil || imageRemoteURL == nil { return nil }
        
        SDWebImageManager.shared().cachedImageExists(for: imageRemoteURL) { (exists) in
            if exists {
                self.loadVideoIfNeeded(completion: completion)
            } else {
                SDWebImageDownloader.shared().downloadImage(with: self.imageRemoteURL, options: [], progress: nil) { (img, _, _, _) in
                    self.image = img
                    self.loadVideoIfNeeded(completion: completion)
                }
            }
        }
    }
    
    private func loadVideoIfNeeded(completion: @escaping ()->Void) {
        if FileManager.default.fileExists(atPath: self.videoPath) {
            completion()
        } else {
            Alamofire.download(self.videoRemoteURL) { (url, response) -> (destinationURL: URL, options: DownloadRequest.DownloadOptions) in
                return (destinationURL: URL(fileURLWithPath: self.videoPath), options: .removePreviousFile)
                }.responseData { (_) in
                    completion()
            }
        }
    }
}


extension FileManager {
    class func videoPath(with remoteURL: URL) -> String {
        let key = remoteURL.lastPathComponent
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(key, isDirectory: false).path
    }
}

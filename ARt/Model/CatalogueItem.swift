//
//  CatalogueItem.swift
//  ARt
//
//  Created by Константин on 11/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


class CatalogueItem {
    
    var title = ""
    var text = ""
    var video = ""
    var artwork = ""
    var id = ""
    
    
    init?(json: [String: Any]?, id _id: String) {
        guard let json = json else { return }
        id = _id
        title = json["title"] as? String ?? ""
        text = json["text"] as? String ?? ""
        video = json["video"] as? String ?? ""
        artwork = json["artwork"] as? String ?? ""
    }
}

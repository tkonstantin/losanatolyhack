//
//  Storage.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import UIKit


class HlocalStorage {
    
    static let shared = HlocalStorage()
    
    var items: [ArtObject] = [] {
        didSet {
            itemsSubscriptions.forEach({ $0(items) })
        }
    }

    
    private var itemsSubscriptions = [([ArtObject])->Void]()
    
    func subscribeForItems(_ block: @escaping (([ArtObject])->Void))  {
        block(items)
        itemsSubscriptions.append(block)
    }
    
}


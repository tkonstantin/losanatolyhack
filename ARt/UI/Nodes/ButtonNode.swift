//
//  ButtonNode.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation

class ButtonNode: SCNNode {
    
    
    func onTapped() {
        self.runAction(SCNAction.sequence([
            SCNAction.scale(to: 0.75, duration: 0.1),
            SCNAction.scale(to: 1.15, duration: 0.1),
            SCNAction.scale(to: 1, duration: 0.1)

            ]))
    }
}

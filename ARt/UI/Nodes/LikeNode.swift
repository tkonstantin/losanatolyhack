//
//  LikeNode.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation
import SVProgressHUD

class LikeNode: ButtonNode {
    
    var object: ArtObject! = nil {
        didSet {
            object.onPostArrived = {
                self.isSelected = self.object.post?.isLiked ?? false
            }
        }
    }
    
    var likesCount: Int = 0
    
    var isSelected = false {
        didSet {
            self.geometry?.firstMaterial?.diffuse.contents = UIImage(named: isSelected ? "like_button_selected" : "like_button")
        }
    }
    
    override func onTapped() {
        super.onTapped()
        
        VK_Proxy.request(isSelected ? VK_Proxy.methods.likes.delete : VK_Proxy.methods.likes.add, parameters: [
            .owner_id : self.object.owner_id,
            .item_id:  self.object.item_id,
            .type: "post"
        ], results: { json in
            if let likes = json?["likes"] as? Int {
                self.likesCount = likes
                self.isSelected = !self.isSelected
            }
        }) { (success, errorMessage) in
            if let error = errorMessage {
                SVProgressHUD.showError(withStatus: error)
            }
        }
    }

}


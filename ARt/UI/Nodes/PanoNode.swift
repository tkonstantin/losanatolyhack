//
//  PanoNode.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation

class PanoNode: SCNNode {
    
    var size: CGSize {
        get {
            return object.image.size
        }
    }
    //    let size = CGSize(width: 1142, height: 1920)
    
    
    private(set) var object: ArtObject!
    private var player: AVPlayer!
    private var videoPlayerNode: SKVideoNode!
    private var planeNode: PanoPlaneNode!
    private var plane: SCNPlane!
    
    var isVisible = true {
        didSet {
            guard isVisible != oldValue  else { return }
//            if isVisible {
//                onAdded()
//            } else {
//                onRemoved()
//            }
        }
    }
    
    enum sides {
        case normal
        case reversed
    }
    

    var canBeFlipped = false
    var isFlippingNow = false
    var side: sides = .normal {
        didSet {
            if side != oldValue && canBeFlipped {
                if isFlippingNow {
                    side = oldValue
                    return
                }
                flip()
            }
        }
    }
    
    
    private func url() ->URL? {
        return URL(fileURLWithPath: object.videoPath)
    }
    
    func setup(object obj: ArtObject) {
        
        self.object = obj
        guard let referenceImage = object?.referenceImage else { return }
        
        plane = SCNPlane(width: referenceImage.physicalSize.width, height: referenceImage.physicalSize.height)
        plane.firstMaterial?.isDoubleSided = true
        
        bubbleNode = nil
        planeNode?.removeFromParentNode()
        planeNode = PanoPlaneNode()
        planeNode.geometry = plane
        
        
        planeNode.eulerAngles.x = -.pi/2
        addChildNode(planeNode)
        
        addVideo()
    }
    
    private func addVideo() {
        guard let url = self.url() else { return }
        
        let playerItem = AVPlayerItem(url: url)
        if let p = self.player {
            NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: p.currentItem)
            p.pause()
        }
        
        self.player = AVPlayer(playerItem: playerItem)
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) { notification in
                self.canBeFlipped = true
            DispatchQueue.main.async {
                self.addLikeButton()
                self.addShareButton()
                self.addPost()
                self.makeBackNodes()
            }
        }
        
        videoPlayerNode = SKVideoNode(avPlayer: player)
        videoPlayerNode.yScale = -1
        
        
        let spriteKitScene = SKScene(size: size)
        spriteKitScene.backgroundColor = UIColor.clear
        spriteKitScene.scaleMode = .aspectFill
        videoPlayerNode.position = CGPoint(x: spriteKitScene.size.width/2, y: spriteKitScene.size.height/2)
        
        videoPlayerNode.size = size
        
        spriteKitScene.addChild(videoPlayerNode)
        planeNode.removeAllActions()
        
        planeNode.geometry?.firstMaterial?.diffuse.contents = spriteKitScene
        makeFading()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.videoPlayerNode.play()
            
        }
    }
    
    private var bubbleNode: SCNNode!
    
    private func addPost() {
        guard bubbleNode == nil else { return }
        
        makeNode(xib: "BubbleXib", xibSize: CGSize(width: 500, height: 119), shouldUpdate: { view in
            (view as? BubbleView)?.update(with: self.object?.post)
        }) { (createdNode) in
            self.bubbleNode = createdNode
            self.planeNode?.addChildNode(self.bubbleNode)
        }
    }
    
    private func makeBackNodes() {
        let backPlane = SCNPlane(width: self.plane.width, height: self.plane.height)
        backPlane.firstMaterial?.isDoubleSided = true
        backPlane.firstMaterial?.diffuse.contents = UIColor.white
        let backPlaneNode = PanoPlaneNode()
        backPlaneNode.geometry = backPlane
        backPlaneNode.simdPosition = .init(x: 0, y: 0, z: -0.025)
        backPlaneNode.runAction(SCNAction.rotateBy(x: 0, y: .pi, z: 0, duration: 0))
        
        
        let xibSize = CGSize(width: 500, height: 300)
        
        DispatchQueue.main.async {
            guard let someView = UINib(nibName: "ProfileView", bundle: nil).instantiate(withOwner: self, options: nil).first as? UIView else { return }
            (someView as? ProfileView)?.update(self.object) {
                
                self.makeNode(xib: "BackView", xibSize: CGSize(width: 500, height: 128), baseNode: GroupButtonNode(), shouldUpdate: { (view) in
                    (view as? BackView)?.update("Перейти в группу ВК", type: .redirect)
                }) { (createdNode) in
                    createdNode.simdPosition = .init(x: 0, y: -0.05 + 0.075, z: 0.025)
                    backPlaneNode.addChildNode(createdNode)
                }
                
                self.makeNode(xib: "BackView", xibSize: CGSize(width: 500, height: 128), baseNode: MusicButtonNode(), shouldUpdate: { (view) in
                    (view as? BackView)?.update("Слушать музыку", type: .music)
                }) { (createdNode) in
                    createdNode.simdPosition = .init(x: 0, y: -0.15 - 0.02 + 0.075, z: 0.025)
                    backPlaneNode.addChildNode(createdNode)
                }
                
                self.makeNode(xib: "BackView", xibSize: CGSize(width: 500, height: 128), baseNode: BuyButtonNode(), shouldUpdate: { (view) in
                    (view as? BackView)?.update("Купить билеты", type: .tickets)
                }) { (createdNode) in
                    createdNode.simdPosition = .init(x: 0, y: -0.25 - 0.04 + 0.075, z: 0.025)
                    backPlaneNode.addChildNode(createdNode)
                }
                
                
                let repeatNode = ReplayButtonNode()
                repeatNode.geometry = SCNPlane(width: 0.05, height: 0.05)
                repeatNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "repeat")
                repeatNode.simdScale = .init(x: 0.01, y: 0.01, z: 0.01)
                repeatNode.runAction(SCNAction.scale(to: 1, duration: 0.2))
                repeatNode.simdPosition = .init(x: 0, y: -0.35 - 0.06 + 0.075, z: 0.025)
                backPlaneNode.addChildNode(repeatNode)
                
                
                
                someView.frame = CGRect(origin: .zero, size: CGSize(width: xibSize.width, height: xibSize.height))
                someView.setNeedsLayout()
                someView.layoutIfNeeded()
                
                DispatchQueue.global(qos: .userInitiated).async {
                    let newNode = SCNNode(geometry: SCNPlane(width: 0.5, height: 0.5 * xibSize.height/xibSize.width))
                    DispatchQueue.main.async {
                        newNode.geometry?.firstMaterial?.diffuse.contents = someView.toImage()
                    }
                    newNode.simdPosition = .init(x: 0, y: 0, z: 0)
                    DispatchQueue.main.async {
                        newNode.simdScale = .init(x: 0.01, y: 0.01, z: 0.01)
                        newNode.runAction(SCNAction.scale(to: 1, duration: 0.2))
                        newNode.simdPosition = .init(x: 0, y: 0.275, z: 0.025)
                        backPlaneNode.addChildNode(newNode)
                    }
                }
                
                self.planeNode.addChildNode(backPlaneNode)

                
            }
         
            
        }
        
  
    }
    
    private func makeNode(xib: String, xibSize: CGSize, baseNode: SCNNode = SCNNode(), shouldUpdate: @escaping (UIView)->Void, completion: @escaping (SCNNode)->Void) {
        DispatchQueue.main.async {
            guard let someView = UINib(nibName: xib, bundle: nil).instantiate(withOwner: self, options: nil).first as? UIView else { return }
            shouldUpdate(someView)
            someView.frame = CGRect(origin: .zero, size: CGSize(width: xibSize.width, height: xibSize.height))
            someView.setNeedsLayout()
            someView.layoutIfNeeded()
            
            DispatchQueue.global(qos: .userInitiated).async {
                let newNode = baseNode
                 newNode.geometry = SCNPlane(width: 0.5, height: 0.5 * xibSize.height/xibSize.width)
                DispatchQueue.main.async {
                    newNode.geometry?.firstMaterial?.diffuse.contents = someView.toImage()
                }
                newNode.simdPosition = .init(x: 0, y: 0, z: 0)
                newNode.simdPosition.y += 0.5
                newNode.simdPosition.z += 0.025
                DispatchQueue.main.async {
                        newNode.simdScale = .init(x: 0.01, y: 0.01, z: 0.01)
                        newNode.runAction(SCNAction.scale(to: 1, duration: 0.2))
                        completion(newNode)
                }
            }
            
        }
    }
    
    private func makeFading() {
        self.opacity = 0
        let fadingSequence = SCNAction.sequence([SCNAction.fadeIn(duration: 2)])
        self.runAction(fadingSequence)
    }
    
    
    private func makeRotating() {
        
    }
    
    func onRemoved() {
        
    }
    
    func onAdded() {
        guard self.object != nil else { return }
        self.setup(object: self.object)
    }
    
    private func addLikeButton() {
        likeButton.removeFromParentNode()
        likeButton.simdScale = .init(x: 0.01, y: 0.01, z: 0.01)
        likeButton.simdPosition = .init(x: 0, y: 0, z: 0)
        likeButton.simdPosition.x -= 0.18
        likeButton.simdPosition.y -= 0.5
        likeButton.simdPosition.z += 0.05
        planeNode.addChildNode(likeButton)
        likeButton.runAction(SCNAction.scale(to: 1, duration: 0.2))
    }
    
    private func addShareButton() {
        shareButton.removeFromParentNode()
        shareButton.simdScale = .init(x: 0.01, y: 0.01, z: 0.01)
        shareButton.simdPosition = .init(x: 0, y: 0, z: 0)
        shareButton.simdPosition.x += 0.18
        shareButton.simdPosition.y -= 0.5
        shareButton.simdPosition.z += 0.05
        planeNode.addChildNode(shareButton)
        shareButton.runAction(SCNAction.scale(to: 1, duration: 0.2))

    }
    
    lazy var likeButton: LikeNode = {
        let newNode = LikeNode()
        newNode.geometry = SCNPlane(width: 0.1, height: 0.1)
        newNode.object = object
        newNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "like_button")
        return newNode
    }()
    
    lazy var shareButton: ShareNode = {
        let newNode = ShareNode()
        newNode.geometry = SCNPlane(width: 0.1, height: 0.1)
        newNode.object = object
        newNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "share_button")
        return newNode
    }()
    
    func flip() {
        isFlippingNow = true
        
        planeNode.runAction(SCNAction.sequence([
            SCNAction.rotateBy(x: 0, y: 0, z: .pi, duration: 0.8),
            SCNAction.move(by: SCNVector3(0, -0.5, 0), duration: 0.5)
            ]))
        
        planeNode.runAction(SCNAction.sequence([
            SCNAction.move(by: SCNVector3(0, 0.5, 0), duration: 0.5)
        ]))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.isFlippingNow = false
        }
    }

}

class PanoPlaneNode: SCNNode { }


class GroupButtonNode: SCNNode {}
class MusicButtonNode: SCNNode {}
class BuyButtonNode: SCNNode {}
class ReplayButtonNode: SCNNode {}

//
//  ShareNode.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation
import SVProgressHUD


class ShareNode: ButtonNode {
    
    var object: ArtObject! = nil {
        didSet {
            object.onPostArrived = {
                self.isSelected = self.object.post?.isShared ?? false
            }
        }
    }
    
    var isSelected: Bool = false  {
        didSet {
            self.geometry?.firstMaterial?.diffuse.contents = UIImage(named: isSelected ? "share_button_tapped" : "share_button")
        }
    }
    
    private func presentShareDialog()  {
        
        let alert = UIAlertController(title: "Ваш комментарий", message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (field) in
            field.placeholder = "Введите текст (необязательно)"
        })
        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: { (_) in
            
            VK_Proxy.request(VK_Proxy.methods.wall.repost, parameters: [.object : "wall\(self.object.owner_id!)_\(self.object.item_id!)", .message: alert.textFields?.first?.text ?? ""], completion: { (success, errorMessage) in
                if success {
                    self.isSelected = true
                    SVProgressHUD.showSuccess(withStatus: "Опубликовано")
                } else {
                    SVProgressHUD.showError(withStatus: errorMessage)
                }
            })
            
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
       
    }
    
    
    override func onTapped() {
        super.onTapped()
        
        if AuthService.isAuthorized {
            presentShareDialog()
        } else {
            AuthService.login { [weak self] in
                self?.presentShareDialog()
            }
        }

    }
    
}


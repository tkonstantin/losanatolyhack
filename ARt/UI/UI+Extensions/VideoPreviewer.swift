//
//  VideoPreviewer.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.

import Foundation
import UIKit
import AVFoundation

extension UIImage {
    
    class func imageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    static func createVideoPreviewFrom(path videoPath: String, completion: @escaping (UIImage?)->Void) {
        DispatchQueue.global().async {
            let imageGenerator = AVAssetImageGenerator(asset: AVAsset(url: URL(fileURLWithPath: videoPath)))
            imageGenerator.appliesPreferredTrackTransform = true
            guard let imageRef = try? imageGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil) else { completion(nil); return; }
            DispatchQueue.main.async {
                completion(UIImage(cgImage:imageRef))
            }
        }
        
    }
    
    static func videoSize(path videoPath: String) -> CGSize {
        return AVAsset(url: URL(fileURLWithPath: videoPath)).tracks(withMediaType: .video).first?.naturalSize ?? CGSize(width: 1920, height: 1080)
    }
    
    static func createVideoPreviewFrom(path videoPath: String) -> UIImage? {
        let imageGenerator = AVAssetImageGenerator(asset: AVAsset(url: URL(fileURLWithPath: videoPath)))
        imageGenerator.appliesPreferredTrackTransform = true
        if let imageRef = try? imageGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil) {
            return UIImage(cgImage:imageRef)
        } else {
            return nil
        }
    }
}

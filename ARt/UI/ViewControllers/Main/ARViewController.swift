//
//  ViewController.swift
//  ARt
//
//  Created by Константин on 23/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation
import SafariServices

class ARViewController: UIViewController, ARSCNViewDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    var player: AVPlayer!
    
    var panoNodes: [PanoNode] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        sceneView.scene = SCNScene()
        
        HlocalStorage.shared.subscribeForItems { [weak self] (itemsSnapshot) in
            self?.items = itemsSnapshot
        }
        
    }
    
    var items: [ArtObject] = [] {
        didSet {
            updateSession()
        }
    }
    
    func updateSession() {
        let config = ARImageTrackingConfiguration.init()
        let trackingImages = items.compactMap({$0.referenceImage})
        config.trackingImages = Set(trackingImages)
        config.maximumNumberOfTrackedImages = items.count
        sceneView.session.run(config, options: [])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sceneView.session.pause()
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
    }
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = PanoNode()
        panoNodes.append(node)
        
        if let img = anchor as? ARImageAnchor {
            if let item = items.first(where: { $0.id == img.referenceImage.name}) {
                
                let basePlane = SCNPlane(width: item.referenceImage!.physicalSize.width, height: item.referenceImage!.physicalSize.height)
                basePlane.firstMaterial?.diffuse.contents = UIColor.clear
                
                node.geometry = basePlane
                node.setup(object: item)
                
                let bgNode = PanoPlaneNode()
                bgNode.geometry = SCNPlane(width: item.referenceImage!.physicalSize.width, height: item.referenceImage!.physicalSize.height)
                bgNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "background")
                bgNode.eulerAngles.x = -.pi/2
                bgNode.position.y = -0.01
                node.addChildNode(bgNode)
            }
            
        }
        return node
    }
    
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        guard let pointOfView = renderer.pointOfView else { return }
        
        for node in panoNodes {
            node.isVisible = renderer.isNode(node, insideFrustumOf: pointOfView)
        }
    }
    
    
    var tappedNode: ButtonNode!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let location = touches.first?.location(in: self.view) else { return }
        if let node = hitNode(for: location) as? ButtonNode {
            tappedNode = node
        } else if let node = hitNode(for: location) as? PanoPlaneNode {
            if let parent = node.parent as? PanoNode ?? node.parent?.parent as? PanoNode {
                parent.side = parent.side == .normal ? .reversed : .normal
            }
        } else if let node = hitNode(for: location) as? GroupButtonNode {
            guard let pano = (node.parent as? PanoNode ?? node.parent?.parent as? PanoNode ?? node.parent?.parent?.parent as? PanoNode) else { return }
            UIApplication.shared.open(URL(string: "https://vk.com/wall\(pano.object!.owner_id!)_\(pano.object!.item_id!)")!)
        } else if let _ = hitNode(for: location) as? MusicButtonNode {
            self.tabBarController?.selectedIndex = 1
        } else if let _ = hitNode(for: location) as? BuyButtonNode {
            //MARK: - пример
            let sf = SFSafariViewController(url: URL(string: "https://bigbilet.ru/ticket-sale/ticket/?owner=BF1EE8A0D65E9803E040115529B054C7&id_service=62238BF68DEE6059E0504B5E01F56877&id_agent=BF1EE8A0D65E9803E040115529B054C7&locale=ru")!)
            self.present(sf, animated: true, completion: nil)
        } else if let node = hitNode(for: location) as? ReplayButtonNode {
            guard let pano = (node.parent as? PanoNode ?? node.parent?.parent as? PanoNode ?? node.parent?.parent?.parent as? PanoNode) else { return }
            pano.side = .normal
            pano.onAdded()
        }

    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        tappedNode?.onTapped()
        tappedNode = nil
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        tappedNode = nil
    }
    
    
    func hitNode(for point: CGPoint) -> SCNNode? {
        return sceneView.hitTest(point, options: [SCNHitTestOption.ignoreChildNodes : false]).last?.node
    }
    
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    
}

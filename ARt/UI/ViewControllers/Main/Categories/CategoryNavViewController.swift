//
//  CategoryNavViewController.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.

import UIKit

class CategoryNavViewController: UINavigationController {
    
    var isGoingToDisappear = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return isGoingToDisappear ? .default : .lightContent
        }
    }

}

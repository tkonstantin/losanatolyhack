//
//  CategoryViewController.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.

import UIKit
import SDWebImage
import AVFoundation


class CategoryViewController: UIViewController {
    
    
    static var currentInstance: CategoryViewController?
    
    class func present(with category: CatalogueItem, delegate: CategoryAnimationDelegate?, dataSource: CategoryAnimationDataSource?, from: UIViewController) {
        DispatchQueue.main.async {
            
            let nav = UIStoryboard(name: "Category", bundle: nil).instantiateInitialViewController() as! UINavigationController
            nav.modalPresentationStyle = .overCurrentContext
            
            let catVC = nav.viewControllers.first as! CategoryViewController
            catVC.category = category
            catVC.delegate = delegate
            catVC.dataSource = dataSource
            currentInstance = catVC
            from.present(nav, animated: false)
            
        }
    }
    
    let imagesAnimationDumping: CGFloat = 0.7
    let animationDuration: Double = 0.5
    var animatingImages: [UIImageView?] = []
    var canDisappear = false
    var appearanceAnimationCompleted = false
    var disappearAnimationStarted = false
    
    
    
    let panDismissalOffset: CGFloat = -256
    var canTrackingPan = true
    var canUpdateStatusMaskWithScroll = true
    var isReallyDragging = false
    
    
    var category: CatalogueItem!
    
    weak var dataSource: CategoryAnimationDataSource?
    weak var delegate: CategoryAnimationDelegate?
    
    
    var artworkFrame: CGRect! {
        get {
            return dataSource?.categoryAnimation_artworkFrame() ?? .zero
        }
    }
    
    lazy var normalArtworkFrame: CGRect = {
        return artwork.frame
    }()
    
    lazy var normalCategoryTitleFrame: CGRect = {
        return category_title.frame
    }()
    
    
    @IBOutlet weak var closeButton: CloseButton!
    @IBOutlet weak var artwork: UIImageView!
    @IBOutlet weak var artwork_mask: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var category_title: UILabel!
    @IBOutlet weak var statusMask: UIView!
    @IBOutlet weak var category_description: UILabel!
    
    var shouldCornerFirstCell = true
    var appearanceAnimationStarted = false
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        _ = normalArtworkFrame
        _ = normalCategoryTitleFrame
        
        configureTable()
        category_title.text = category.title
        category_description.text = category.text
        artwork.setImage(with: category.artwork, animateProgress: false)
        animateAppearance()
    }
    
    
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        player = AVPlayer(url: Bundle.main.url(forResource: "videoSample", withExtension: "mp4")!)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.backgroundColor = UIColor.black.cgColor
        playerLayer.frame.size = CGSize(width: self.artwork.bounds.height*16/9, height: self.artwork.bounds.height)
        playerLayer.frame.origin = CGPoint(x: (self.artwork.bounds.width-(self.artwork.bounds.height*16/9))/2, y: 0)
        artwork_mask.layer.addSublayer(playerLayer)
    }
    
    var isPlaying = false {
        didSet {
            if isPlaying {
                player.play()
            } else {
                player.pause()
            }
        }
    }
    
    @IBAction func playTapped(_ sender: UIButton) {
        isPlaying = !isPlaying
        if isPlaying {
            sender.setImage(nil, for: .normal)
        } else {
            sender.setImage(UIImage(named: "videPlayIcon"), for: .normal)

        }
    }
    
    
    @IBAction func closeTapped(_ sender: UIButton) {
        disappear()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let presented = self.presentedViewController {
                return presented.preferredStatusBarStyle
            } else {
                return .default
            }
        }
    }
    
    
    var dismissalProgress: CGFloat = 0 {
        didSet {
            guard canTrackingPan, dismissalProgress != oldValue, appearanceAnimationCompleted, !disappearAnimationStarted else { return }
            
            let currentProgress = dismissalProgress/panDismissalOffset
            
            if dismissalProgress == 0 {
                //                canTrackingPan = false
                UIView.animate(withDuration: animationDuration, animations: {
                    
                    self.artwork.frame = self.normalArtworkFrame
                    self.artwork_mask.alpha = 1 - currentProgress*2
                    self.statusMask.alpha = 0
                    self.artwork.cornerRadius = 0.01
                    self.category_title.center.y = self.artwork.center.y
                }, completion: {_ in
                    //                    self.canTrackingPan = true
                    self.canUpdateStatusMaskWithScroll = true
                })
            } else {
                self.canUpdateStatusMaskWithScroll = false
                self.statusMask.alpha = abs(dismissalProgress/20)
                self.artwork_mask.alpha = 1 - currentProgress*2
                self.artwork.cornerRadius = currentProgress*16
            }
            
            
            let xWidth = self.normalArtworkFrame.size.width
            let yWidth = self.artworkFrame.size.width
            let targetWidth = xWidth*(1 - currentProgress) + yWidth*currentProgress
            
            let xHeight = self.normalArtworkFrame.size.height
            let yHeight = self.artworkFrame.size.height
            let targetHeight = xHeight*(1 - currentProgress) + yHeight*currentProgress
            
            artwork.frame = CGRect(x: self.artworkFrame.origin.x*currentProgress,
                                   y: min(self.artworkFrame.origin.y, 64)*currentProgress,
                                   width: targetWidth,
                                   height: targetHeight)
            
            self.category_title.center.y = self.artwork.center.y
            
            if currentProgress >= 0.5 {
                
                self.disappear()
            }
        }
    }
    
}

extension CategoryViewController  {
    
    func configureTable() {
        DispatchQueue.main.async {
            self.tableView.tableHeaderView?.frame.size.height = self.artwork.bounds.height + self.category_title.frame.size.height + self.category_description.frame.size.height + 48
        }
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        isReallyDragging = false
        self.dismissalProgress = 0
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isReallyDragging = false
        self.dismissalProgress = 0
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isReallyDragging = true
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        isReallyDragging = false
        self.dismissalProgress = 0
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard !disappearAnimationStarted else { return }
        
        if scrollView.contentOffset.y > 0 {
            self.dismissalProgress = 0
        }
        let needsToUpdateViews = (self.navigationController as? CategoryNavViewController)?.isGoingToDisappear != true
        
        if scrollView.contentOffset.y <= 0 {
            if isReallyDragging  {
                self.dismissalProgress += scrollView.contentOffset.y
            }
            scrollView.contentOffset.y = 0
        }
        
        if needsToUpdateViews {
            
            if canUpdateStatusMaskWithScroll {
                let isStatusBarIntersected = scrollView.contentOffset.y > offsetIntersectingStatus
                self.statusMask.alpha = isStatusBarIntersected ? 1 : 0
            }
            
            let isCloseButtonIntersected = scrollView.contentOffset.y > self.tableView.tableHeaderView!.bounds.height - closeButton.frame.maxY
            self.closeButton.setMode(isCloseButtonIntersected ? .dark : .light)
        }
    }
    
    var offsetIntersectingStatus: CGFloat {
        get {
            if #available(iOS 11.0, *) {
                return self.tableView.tableHeaderView!.bounds.height - self.view.safeAreaInsets.top
            } else {
                return self.tableView.tableHeaderView!.bounds.height - 20
            }
        }
    }
    
    
}


extension CategoryViewController { //animations
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        CategoryViewController.currentInstance = nil
        super.dismiss(animated: flag, completion: completion)
    }
    
    func animateAppearance() {
        
        guard !appearanceAnimationStarted else { self.tableView.tableFooterView = nil; return }
        self.view.isUserInteractionEnabled = false
        
        appearanceAnimationStarted = true
        self.setNeedsStatusBarAppearanceUpdate()
        delegate?.categoryAnimation_shouldAppear(duration: animationDuration/2)
        
        artwork_mask.frame = artworkFrame
        artwork.frame = artworkFrame
        artwork.isHidden = false
        
        turnAutoLayoutOff(for: [self.artwork, self.artwork_mask, self.category_title])
        
        
        
        UIView.animate(withDuration: self.animationDuration/2) {
            self.tableView.alpha = 1
            self.view.backgroundColor = .white
        }
        
        UIView.animate(withDuration: self.animationDuration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [.curveEaseIn], animations: {
            self.category_title.alpha = 1
            self.category_description.alpha = 1
            self.closeButton.alpha = 1
            self.artwork.cornerRadius = 0.01
            self.artwork_mask.cornerRadius = 0.01
            self.artwork_mask.alpha = 1
            self.artwork.frame = self.normalArtworkFrame
            self.artwork_mask.frame = self.normalArtworkFrame
            self.category_title.center.y = self.artwork.center.y
        }) { (_) in
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100), execute: {
                self.view.isUserInteractionEnabled = true
                self.canDisappear = true
                self.appearanceAnimationCompleted = true
            })
        }
    }
    
    func disappear(completion: (()->Void)?=nil) {
        self.canUpdateStatusMaskWithScroll = true
        self.canTrackingPan = false
        guard canDisappear, !disappearAnimationStarted else { return }
        self.view.isUserInteractionEnabled = false
        self.disappearAnimationStarted = true
        canDisappear = false
        (self.navigationController as? CategoryNavViewController)?.isGoingToDisappear = true
        self.setNeedsStatusBarAppearanceUpdate()
        self.statusMask.alpha = 0
        
        self.category_title.alpha = 0
        self.category_description.alpha = 0
        self.closeButton.alpha = 0
        
        
        turnAutoLayoutOff(for: [self.tableView])
        
        UIView.animate(withDuration: animationDuration/2) {
            self.view.backgroundColor = .clear
            if self.dismissalProgress == 0 {
                self.tableView.frame = self.tableView.frame.offsetBy(dx: 0, dy: self.view.bounds.height)
            } else {
                self.tableView.alpha = 0
            }
        }
        
        
        
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.2, options: [.curveEaseIn], animations: {
            self.artwork.cornerRadius = 8
            self.artwork_mask.cornerRadius = 8
            self.artwork_mask.alpha = 0
            self.artwork.frame = self.artworkFrame
            self.artwork_mask.frame = self.artworkFrame
        }, completion: {_ in
            self.delegate?.categoryAnimation_shouldDisappear(duration: self.animationDuration)
            self.dismiss(animated: false, completion: {
                completion?()
            })
        })
    }
    
    private func animationDuration(for index: Int) -> Double {
        return animationDuration*(Double(index)+1)/2
        
    }
    
    private var defaultInvisibleFrame: CGRect {
        get {
            return CGRect(x: 16, y: UIScreen.main.bounds.height, width: 44, height: 44)
        }
    }
    
    
    func turnAutoLayoutOff(for views: [UIView]) {
        for v in views {
            let currentFrame = v.frame
            v.constraints.forEach({$0.isActive = false})
            v.translatesAutoresizingMaskIntoConstraints = true
            v.frame = currentFrame
        }
    }
    
    
}

//
//  CategoryCollectionViewCell.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class SearchCollectionViewCell: AnimatableCollectionCell {
    
    static let cell_id = "categoryCell"
    
    @IBOutlet weak var artwork: UIImageView!
    @IBOutlet weak var title_label: UILabel!
    
    @IBOutlet weak var blurView: UIVisualEffectView! {
        didSet {
            normalBlurAlpha = blurView.alpha
        }
    }
    
    override var viewToHide: UIView? {
        get {
            return blurView
        }
    }
    
    func update(with category: CatalogueItem) {
        DispatchQueue.main.async {
            self.title_label.text = category.title
            self.artwork.setImage(with: category.artwork, animateProgress: true)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        guard self.traitCollection.forceTouchCapability == .available else { return }
        
        guard let touch = touches.first else { return }
        let forceConverted = ((touch.force < 1 ? 1 : touch.force) - 1)/100
        
        animate(block: {
            self.transform = CGAffineTransform(scaleX: 1 - forceConverted, y: 1 - forceConverted)
        }, completion: {
        })
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        
        guard self.traitCollection.forceTouchCapability == .available else { return }
        
        
        guard let touch = touches.first else { return }
        let forceConverted = ((touch.force < 1 ? 1 : touch.force) - 1)/100
        self.transform = CGAffineTransform(scaleX: 1 - forceConverted, y: 1 - forceConverted)
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard self.traitCollection.forceTouchCapability == .available else { super.touchesEnded(touches, with: event); return }
        
        
        animate(block: {
            self.transform = .identity
        }, completion: {
            super.touchesEnded(touches, with: event)
        })
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard self.traitCollection.forceTouchCapability == .available else { super.touchesCancelled(touches, with: event); return }
        
        animate(block: {
            self.transform = .identity
        }, completion: {
            super.touchesCancelled(touches, with: event)
        })
    }
    
    
    let animationDuration: Double = 0.1
    
    func animate(duration: Double = 0, block: @escaping()->Void, completion: @escaping ()->Void) {
        let targetDuration = duration == 0 ? animationDuration : duration
        UIView.animate(withDuration: targetDuration, animations:  {
            block()
        }, completion: { _ in
            completion()
        })
        
    }
    
}

//
//  SearchViewController.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.

import UIKit

class SearchViewController: UIViewController {
    

    @IBOutlet weak var blurMask: UIVisualEffectView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var categories_title: UILabel!
  
    
    var categories: [CatalogueItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollection()
        
        (UIApplication.shared.delegate as? AppDelegate)?.server?.loadCatalogue(completion: { (items) in
            self.categories = items
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        })
        
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let presented = self.presentedViewController {
                return presented.preferredStatusBarStyle
            } else {
                return .default
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        var currentCollectionItem: IndexPath?
        if let visible = self.collectionView?.indexPathsForVisibleItems {
            if visible.count == 3 {
                currentCollectionItem = visible.sorted()[1]
            } else if visible.count == 2 {
                if visible.contains(IndexPath(item: 0, section: 0)) {
                    currentCollectionItem = visible.sorted().first
                } else {
                    currentCollectionItem = visible.sorted().last
                }
            }
        }
        blurMask?.isHidden = false
        
        coordinator.animate(alongsideTransition: nil) { (_) in
            self.configureCollection()
            self.collectionView?.scrollToItem(at: currentCollectionItem ?? IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: false)
            self.blurMask?.isHidden = true
        }
    }
    
    var selectedCollectionPath: IndexPath?
    
}


extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func configureCollection() {
        DispatchQueue.main.async {
            let margin: CGFloat = 24
            self.collectionView?.collectionViewLayout = CellBasedPagingLayout()
            guard let layout = self.collectionView?.collectionViewLayout as? CellBasedPagingLayout else { return }
            layout.minimumLineSpacing = 0
            layout.contentInset = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin - layout.minimumLineSpacing)
            
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            
            self.collectionView.showsHorizontalScrollIndicator = false
            self.collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
            
            self.collectionView.reloadData()
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchCollectionViewCell.cell_id, for: indexPath) as! SearchCollectionViewCell
        cell.update(with: categories[indexPath.item])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCollectionPath = indexPath
        CategoryViewController.present(with: self.categories[indexPath.item], delegate: self, dataSource: self, from: self)
        
    }

    
}


extension SearchViewController: CategoryAnimationDataSource {
    func categoryAnimation_artworkFrame() -> CGRect {
        guard let indexPath = self.selectedCollectionPath else { return .zero }
        let cell = collectionView.cellForItem(at: indexPath) as! SearchCollectionViewCell
        var convertedFrame = self.collectionView.convert(cell.frame, to: self.view)
        convertedFrame.origin.x += 8
        convertedFrame.origin.y += 8
        convertedFrame.size.width -= 16
        convertedFrame.size.height -= 16
        return convertedFrame
    }

}
extension SearchViewController: CategoryAnimationDelegate {
    
    func categoryAnimation_shouldDisappear(duration: Double) {
        guard let indexPath = self.selectedCollectionPath else { return }
        guard let cell = collectionView.cellForItem(at: indexPath) as? SearchCollectionViewCell else { return }
        cell.appear(duration)
        self.mainTab?.showBar()
    }
    
    func categoryAnimation_shouldAppear(duration: Double) {
        guard let indexPath = self.selectedCollectionPath else { return }
        guard let cell = collectionView.cellForItem(at: indexPath) as? SearchCollectionViewCell else { return }
        cell.disappear(duration)
        self.mainTab?.hideBar()
    }
}





extension UIViewController {
    
    var mainTab: UITabBarController? {
        get {
            return self.tabBarController
        }
    }
}

extension UITabBarController {
    
    
    func hideBar() {
        if defaultBarHeight == 0 {
            defaultBarHeight = self.tabBar.frame.size.height
        }
        UIView.animate(withDuration: 0.2) {
            self.tabBar.frame.origin.y = self.view.bounds.height
        }
    }
    
    func showBar() {
        UIView.animate(withDuration: 0.2) {
            self.tabBar.frame.origin.y = self.view.bounds.height - defaultBarHeight
        }
    }
}

var defaultBarHeight: CGFloat = 0


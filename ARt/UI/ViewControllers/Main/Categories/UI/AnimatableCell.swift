//
//  AnimatableCell.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


 class AnimatableCell: UITableViewCell {
    
      var viewToHide: UIView? {
        get {
            return nil
        }
    }
    
     var normalBlurAlpha: CGFloat = 1
    
     func appear( _ duration: Double) {
        self.alpha = 1
        UIView.animate(withDuration: duration) {
            self.viewToHide?.alpha = self.normalBlurAlpha
        }
    }
    
     func disappear( _ duration: Double=0) {
        self.alpha = 0
        self.viewToHide?.alpha = 0
    }
}

 class AnimatableCollectionCell: UICollectionViewCell {
    
     var viewToHide: UIView? {
        get {
            return nil
        }
    }
    
     var normalBlurAlpha: CGFloat = 1
    
     func appear( _ duration: Double) {
        self.alpha = 1
        UIView.animate(withDuration: duration) {
            self.viewToHide?.alpha = self.normalBlurAlpha
        }
    }
    
     func disappear( _ duration: Double=0) {
        self.alpha = 0
        self.viewToHide?.alpha = 0
    }
}




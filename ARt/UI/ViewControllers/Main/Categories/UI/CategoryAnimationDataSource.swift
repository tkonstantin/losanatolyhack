//
//  CategoryAnimationDataSource.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

 protocol CategoryAnimationDataSource: class {
    func categoryAnimation_artworkFrame() -> CGRect
}

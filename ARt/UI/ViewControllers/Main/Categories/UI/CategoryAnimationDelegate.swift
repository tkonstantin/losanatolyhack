//
//  CategoryAnimationDelegate.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

 protocol CategoryAnimationDelegate: class {
    func categoryAnimation_shouldAppear(duration: Double)
    func categoryAnimation_shouldDisappear(duration: Double)
}

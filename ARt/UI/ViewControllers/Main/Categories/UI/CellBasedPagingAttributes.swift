//
//  CellBasedPagingAttributes.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class CellBasedPagingAttributes: UICollectionViewLayoutAttributes {
    
    
    override func copy(with zone: NSZone? = nil) -> Any {
        let attribute = super.copy(with: zone) as! CellBasedPagingAttributes
        return attribute
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? CellBasedPagingAttributes else { return false }
        return super.isEqual(object)
    }
}


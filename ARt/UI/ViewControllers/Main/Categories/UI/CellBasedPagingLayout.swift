//
//  CellBasedPagingLayout.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.


import UIKit

class CellBasedPagingLayout: UICollectionViewLayout {
    
    lazy var itemSize: CGSize = {
        guard let collection = collectionView else { return .zero}
        return CGSize(width: collection.bounds.width - 48, height: collection.bounds.height)
        
    }()
    
    var minimumLineSpacing: CGFloat = 20
    var baseLayoutAttributes = [CellBasedPagingAttributes]()
    var contentInset: UIEdgeInsets = .zero
    
    
    
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = self.collectionView else {
            return proposedContentOffset
        }
        
        let proposedRect = CGRect(x: proposedContentOffset.x, y: 0, width: collectionView.bounds.width, height: collectionView.bounds.height)
        guard let layoutAttributes = self.layoutAttributesForElements(in: proposedRect) else {
            return proposedContentOffset
        }
        
        var candidateAttributes: UICollectionViewLayoutAttributes?
        let proposedContentOffsetCenterX = proposedContentOffset.x + collectionView.bounds.width / 2
        
        for attributes in layoutAttributes {
            if attributes.representedElementCategory != .cell {
                continue
            }
            
            if candidateAttributes == nil {
                candidateAttributes = attributes
                continue
            }
            
            if fabs(attributes.center.x - proposedContentOffsetCenterX) < fabs(candidateAttributes!.center.x - proposedContentOffsetCenterX) {
                candidateAttributes = attributes
            }
        }
        
        guard let aCandidateAttributes = candidateAttributes else {
            return proposedContentOffset
        }
        
        var newOffsetX = aCandidateAttributes.center.x - collectionView.bounds.size.width / 2
        let offset = newOffsetX - collectionView.contentOffset.x
        
        if (velocity.x < 0 && offset > 0) || (velocity.x > 0 && offset < 0) {
            let pageWidth = self.itemSize.width + self.minimumLineSpacing
            newOffsetX += velocity.x > 0 ? pageWidth : -pageWidth
        }
        
        return CGPoint(x: newOffsetX, y: proposedContentOffset.y)
    }
    
    
    
    override class var layoutAttributesClass: AnyClass {
        return CellBasedPagingAttributes.self
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        baseLayoutAttributes = [CellBasedPagingAttributes]()
        let nbItems = collectionView?.numberOfItems(inSection: 0) ?? 0
        
        for item in 0..<nbItems {
            let indexPath = IndexPath(item: item, section: 0)
            let yPosition = (collectionView!.frame.height - itemSize.height)/2.0
            let position = CGPoint( x: contentInset.left + ((itemSize.width+minimumLineSpacing) * CGFloat(item)), y:yPosition)
            let frame = CGRect(origin: position, size: itemSize)
            let attributes = CellBasedPagingAttributes(forCellWith: indexPath)
            attributes.frame = frame
            baseLayoutAttributes.append(attributes)
        }
    }

    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {

        guard case let newAttributesArray as [CellBasedPagingAttributes] = NSArray(array: baseLayoutAttributes.filter({$0.frame.intersects(rect)}), copyItems: true) else {
            return nil
        }


        return newAttributesArray
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return baseLayoutAttributes[indexPath.row]
    }
    
    
    override var collectionViewContentSize: CGSize {
        get {
            let totalWidth = CGFloat(baseLayoutAttributes.count) * (itemSize.width + self.minimumLineSpacing) + (contentInset.left + contentInset.right)
            return CGSize(width: totalWidth, height: itemSize.height)
        }
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }

}

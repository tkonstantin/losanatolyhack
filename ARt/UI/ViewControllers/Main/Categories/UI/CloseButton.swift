//
//  CloseButton.swift
//  ARt
//
//  Created by Константин on 11/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

 class CloseButton: ResizableButton {
    
    enum modes {
        case light
        case dark
    }
    
    @IBInspectable var isLight: Bool = true {
        didSet {
            self.setMode(isLight ? .light : .dark)
        }
    }
    
    private var mode: modes = .light {
        didSet {
            update()
        }
    }
    
    private func update() {
        self.backgroundColor = (mode == .dark ? UIColor(hex: "333333") : UIColor.white).withAlphaComponent(0.5)
        self.tintColor = mode == .dark ? UIColor.white : UIColor(hex: "333333")
    }
    
    func setMode( _ newMode: CloseButton.modes) {
        if mode != newMode {
            mode = newMode
        }
    }
}


 class ResizableButton: UIButton {
    
    var originalCenter: CGPoint! = nil
    let animationStep: Double = 0.1
    
    override var isHighlighted: Bool {
        didSet {
            
            if originalCenter == nil {
                originalCenter = self.center
            }
            
            
            if state == .highlighted {
                decreaseAnimated()
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(animationStep*1000)), execute: {
                    self.increaseAnimated()
                })
            }
        }
    }
    
    
    @objc func decreaseAnimated() {
        UIView.animate(withDuration: animationStep, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.1, options: [], animations: {
            self.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
            self.center = self.originalCenter
        }, completion: { _ in
        })
    }
    
    @objc func increaseAnimated() {
        UIView.animate(withDuration: animationStep*2, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.2, options: [], animations: {
            self.transform = .identity
            self.center = self.originalCenter
        }, completion: { _ in
        })
    }
    
}

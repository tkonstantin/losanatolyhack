//
//  UIImageView+Progress.swift
//  ARt
//
//  Created by Константин on 11/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func setImage(with urlString: String, placeholderImage: UIImage?=nil, animateProgress: Bool, needsBackground: Bool = false, completion: ((UIImage?, Bool)->Void)?=nil) {
        
        guard let url = URL(string: urlString) else { completion?(nil, false); return }
        
        DispatchQueue.main.async {
            
            self.sd_setImage(with: url, placeholderImage: placeholderImage, options: []) { (img, err, _, _) in
                completion?(img, err == nil)
            }
        }
        
    }

}

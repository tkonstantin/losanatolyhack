//
//  BackView.swift
//  ARt
//
//  Created by Константин on 10/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class BackView: UIView {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    enum types  {
        case redirect
        case music
        case tickets
        
        var imageName: String {
            switch self {
            case .redirect: return "redirect"
            case .music: return "music"
            case .tickets: return "tickets"
            }
        }
    }
    
    var type: types = .redirect
    
    func update(_ text: String, type t: types) {
        self.type = t
        label.text = text
        icon.image = UIImage(named: type.imageName)
    }
}

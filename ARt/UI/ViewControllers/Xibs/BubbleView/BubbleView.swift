//
//  BubbleView.swift
//  ARt
//
//  Created by Константин on 10/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class BubbleView: UIView {
    
    @IBOutlet weak var likesCount: UILabel!
    @IBOutlet weak var commentsCount: UILabel!
    @IBOutlet weak var viewsCount: UILabel!

    
    func update(with post: VK_Post?) {
        likesCount.text = String(post?.likesCount ?? 0)
        commentsCount.text = String(post?.commentsCount ?? 0)
        viewsCount.text = String(post?.sharesCount ?? 0)
    }

}

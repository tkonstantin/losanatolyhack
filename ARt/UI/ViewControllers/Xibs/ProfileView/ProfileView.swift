//
//  ProfileView.swift
//  ARt
//
//  Created by Константин on 11/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import SDWebImage


class ProfileView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    

    func update(_ object: ArtObject, loadingCompletion: @escaping ()->Void) {
        nameLabel.text = object.title
 
        imageView?.sd_setImage(with: URL(string: object.avatarURL)) { (img, _, _, _) in
            self.imageView.image = img
            DispatchQueue.main.async {
                loadingCompletion()
            }
        }
    }
}

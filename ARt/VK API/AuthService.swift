//
//  AuthService.swift
//  ARt
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import VK_ios_sdk
import UIKit


class AuthService: NSObject {
    
    private static let instance = AuthService()
    
    private static let scope = [VK_PER_WALL, VK_PER_PHOTOS, VK_PER_FRIENDS]

    private var loginCompletion: (()->Void)?=nil
    
    static var isAuthorized: Bool {
        return VKSdk.isLoggedIn()
    }
    
    class func onDidFinishLaunching() {
        VKSdk.initialize(withAppId: "6747171")
        instance.setup()
        VKSdk.wakeUpSession(scope) { _, _ in }
    }
    
    class func handleOpenURL(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) {
        guard let source = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String else { return }
        VKSdk.processOpen(url, fromApplication: source)
    }
    
    class func login(completion: (()->Void)?=nil) {
        instance.loginCompletion = completion
        VKSdk.authorize(scope)
    }
    
    private func setup() {
        VKSdk.instance().register(self)
        VKSdk.instance().uiDelegate = self
    }
}

extension AuthService: VKSdkDelegate, VKSdkUIDelegate {
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        
        UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true, completion:  nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        UIApplication.shared.keyWindow?.rootViewController?.present(VKCaptchaViewController(), animated: true, completion: nil)
    }
    
    func vkSdkDidDismiss(_ controller: UIViewController!) {
        print("did dismiss")
    }
    
    func vkSdkWillDismiss(_ controller: UIViewController!) {
        print("will dismiss")
    }
    
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) { }

    
    func vkSdkUserAuthorizationFailed() {
        print(#function)
    }
    
    func vkSdkTokenHasExpired(_ expiredToken: VKAccessToken!) { }
    
    func vkSdkAuthorizationStateUpdated(with result: VKAuthorizationResult!) { }
    
    
    func vkSdkAccessTokenUpdated(_ newToken: VKAccessToken!, oldToken: VKAccessToken!) { }
}

//
//  VK_Post.swift
//  ARt
//
//  Created by Константин on 10/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


class VK_Post {
    
    var likesCount = 0
    var isLiked = false
    var sharesCount = 0
    var isShared = false
    var commentsCount = 0
    
    var date: Date?
    var text: String?
    var viewsCount = 0
    var attachments: [VK_Attachment] = []
    
    
    init?(json: NSDictionary?) {
        guard let json = json else { return nil }
        
        if let likes = json["likes"] as? NSDictionary {
            likesCount = likes["count"] as? Int ?? 0
            isLiked = likes["user_likes"] as? Bool ?? false
        }
        if let shares = json["reposts"] as? NSDictionary {
            sharesCount = shares["count"] as? Int ?? 0
            isShared = shares["user_reposted"] as? Bool ?? false
        }
        if let comments = json["comments"] as? NSDictionary {
            commentsCount = comments["count"] as? Int ?? 0
        }
        text = json["text"] as? String ?? ""
        if let seconds = json["date"] as? Double {
            date = Date(timeIntervalSince1970: seconds)
        }
        viewsCount = (json["views"] as? NSDictionary)?["count"] as? Int ?? 0
        attachments = (json["attachments"] as? [NSDictionary] ?? []).compactMap({VK_Attachment(json: $0)})
    }
}




class VK_Attachment {
    
    enum types: String {
        case photo = "photo"
        case posted_photo = "posted_photo"
        case video = "video"
        case audio = "audio"
        case doc = "doc"
        case graffiti = "graffiti"
        case link = "link"
        case note = "note"
        case app = "app"
        case poll = "poll"
        case page = "page"
        case album = "album"
        case photos_list = "photos_list"
        case market = "market"
        case market_album = "market_album"
        case sticker = "sticker"
        case pretty_cards = "pretty_cards"
        case empty = ""
    }
    
    var type: types = .empty
    var imageLink: String? //photo_604
    
    init?(json: NSDictionary?) {
        guard let json = json, let rawType =  json["type"] as? String else { return nil }
        type = types(rawValue: rawType) ?? .empty
        
        if let object = json[rawType] as? NSDictionary {
            imageLink = object["photo_604"] as? String
        }
    }
    
}

//
//  VK_Proxy.swift
//  ARt
//
//  Created by Константин on 10/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import VK_ios_sdk


class VK_Proxy {
    
    enum methods {
        enum wall {
            static let repost = "wall.repost"
            static let getByID = "wall.getById"
        }
        enum likes {
            static let add = "likes.add"
            static let delete = "likes.delete"
            static let isLiked = "likes.isLiked"
        }
    }
    
    enum parameters: String {
        case object = "object"
        case message = "message"
        case type = "type"
        case owner_id = "owner_id"
        case posts = "posts"
        case item_id = "item_id"
        case access_key = "access_key"
    }
    
    class func request(_ method: String, parameters params: [parameters: String], results: ((NSDictionary?)->Void)?=nil, completion: @escaping (Bool, String?)->Void) {
        var parameters = [String: String]()
        params.forEach({parameters[$0.key.rawValue] = $0.value})
        
        guard let req = VKRequest(method: method, parameters: parameters) else { completion(false, nil); return }
        
        req.execute(resultBlock: { (response) in
            
            let result = response?.getValue(for: "success") as? Bool ?? false
            results?(response?.json as? NSDictionary ?? (response?.json as? [NSDictionary])?.first)
            
            if result {
                completion(true, nil)
            } else {
                completion(false, nil)
            }
        }, errorBlock: { (error) in
            completion(false, error?.localizedDescription)
        })
    }
    
    
}


extension VKResponse {
    
    @objc func getValue(for key: String) -> Any? {
        return (self.json as? NSDictionary)?[key]
    }
}
